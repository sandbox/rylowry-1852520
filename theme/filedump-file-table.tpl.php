<?php 
global $base_dir;
global $base_url;
?>

<?php foreach ($files as $file): ?>
<?php 
  $classes = array('file-wrapper', 'contextual-links-region');
?>
<div id="file-<?php print $file->fid ?>"class="<?php print implode(' ', $classes) ?>">
  <?php if($ops == TRUE && filedump_access('operations')): ?>
    <div class="contextual-links-wrapper"><ul class="contextual-links" style="display: none;">
    <?php if (filedump_access('delete')): ?>
      <li class="menu-list"><?php print l('Delete', $base_dir . 'f/' . $file->fid, array('attributes' => array('class' => 'filedump-op-delete'))) ?></li>
    <?php endif ?>
    </ul></div>
  <?php endif ?>
  <div class="file-name">
    <?php print l($file->filename, file_create_url($file->uri)) ?>
  </div>
  <div class="file-preview">
    <?php if ($file->filemime == 'image/jpeg' || $file->filemime == 'image/png' || $file->filemime == 'image/gif'): ?>
      <a href="<?php print file_create_url($file->uri) ?>">
        <img src="<?php print image_style_url('thumbnail', $file->uri) ?>" alt="<?php print $file->filename ?>"/>
      </a>
    <?php endif ?>
  </div>
</div>
<?php endforeach ?>
