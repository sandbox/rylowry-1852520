<?php if (isset($file)):?>
  <div id="file-<?php print $file->fid ?>" class="file-wrapper">
    <div class="file-name">
      <?php if($show_checkbox): ?>
        <?php
          print theme('checkbox', array('element' => array(
            '#attributes' => array(
              'id' => 'file-checkbox-' . $file->fid,
              'name' => 'fids[]',
              'value' => $file->fid,
            ),
          )))
        ?>
        <label for="file-checkbox-<?php print $file->fid ?>" title="<?php print $file->filename ?>"><?php print $file->filename ?></label>
      <?php endif ?>
      <?php if ($show_links && $show_checkbox == FALSE): ?>
        <?php print l($file->filename, file_create_url($file->uri), array('attributes' => array('title' => $file->filename))) ?>
      <?php else:?>
        <?php print $file->filename ?>
      <?php endif ?>
    </div>
    <div class="file-preview">
      <?php if($show_links): ?>
        <a href="<?php print file_create_url($file->uri) ?>" title="<?php print $file->filename ?>">
          <?php print $preview ?>
        </a>
      <?php else: ?>
        <?php print $preview ?>
      <?php endif ?>
    </div>
  </div>
<?php endif ?>