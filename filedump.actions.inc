<?php

/**
 * The abstract class that all FileActions must inherit from.
 */
abstract class FileAction {
  
  /**
   * Determines whether a user has permission to use this file action or not.
   * @param unknown_type $user
   */
  public function access($user) {
    return TRUE;
  }
  
  /**
   * Creats a form for the file action.  The list of files being operated
   * on is listed in $form_state['files'].
   * 
   * @param unknown_type $form
   * @param unknown_type $form_state
   * @return array A renderable form array.
   */
  public abstract function getForm($form, &$form_state);
  
  /**
   * Performs ajax for this action.  Use the function filedump_action_ajax()
   * for #ajax['callback'] in the form to execute it.
   * 
   * @param unknown_type $form
   * @param unknown_type $form_state
   */
  public function ajax($form, &$form_state) {
    return array();
  }
  
  /**
   * Validates this file action's form.
   * 
   * @param unknown_type $form
   * @param unknown_type $form_state
   */
  public function validate($form, &$form_state) {
    return TRUE;
  }
  
  /**
   * Called during the successful submission of this file action's form.
   * The list of files being operated on is listed in $form_state['files'].
   *
   * @param unknown_type $form
   * @param unknown_type $form_state
   */
  public abstract function execute($form, &$form_state);
}

class DeleteFileAction extends FileAction {
  
  public function access($user) {
    return filedump_access('delete', $user);
  }
  
  public function getForm($form, &$form_state) {
    // TODO: Include file usage details.
    return array(
      'message' => array(
        '#markup' => '<div>Are you sure you want to delete the following files?</div>',
      ),
      'files' => array(
        '#theme' => 'filedump_file_table',
        '#files' => &$form_state['files'],
      ),
      'wrapper' => array(
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        'yes' => array(
          '#type' => 'submit',
          '#value' => t('Yes')
        ),
        'cancel' => array(
          '#type' => 'submit',
          '#value' => t('Cancel'),
        ),
      ),
    );
  }
  
  public function execute($form, &$form_state) {
    if ($form_state['values']['op'] == 'Yes') {
      $file_count = count($form_state['files']);
      foreach ($form_state['files'] as $file) {
        file_delete($file, TRUE);
      }
      if ($file_count == 1) {
        drupal_set_message(t('@file has been deleted.', array('@file' => $form_state['files'][0]->filename)));
      }
      else {
        drupal_set_message(t('@file_count files have been deleted.', array('@file_count' => $file_count)));
      }
    }
    else {
      drupal_set_message(t('File deletion cancelled.  No files have been removed.'));
    }
    $form_state['redirect'] = 'filedump';
  }
  
}

class CreateNodesAction extends FileAction {

  public function ajax($form, &$form_state) {
    return $form['file_field_wrapper']['file_field'];
  }
  
  public function getForm($form, &$form_state) {
    $form = array();
    $form['file_list'] = array(
      '#theme' => 'filedump_file_select',
      '#files' => $form_state['files'],
      '#show_checkboxes' => FALSE,
      '#show_links' => FALSE,
      '#view_mode' => 'thumbs',
    );
    
    // Step 1: Ask the user what kind of node to create
    if (!isset($form_state['node_type'])) {
      $bundles = node_type_get_types();
      $node_type_options = array('' => t('Choose a node type'));
      
      foreach ($bundles as $bundle) {
        $instances = field_info_instances('node', $bundle->type);
        foreach ($instances as $key => $instance) {
          $info = field_info_field($key);
          if ($info['type'] == 'image' || $info['type']  == 'file') {
            $node_type_options[$bundle->type] = $bundle->name;
          }
        }
      }
      
      $form += array(
        'node_type' => array(
          '#type' => 'select',
          '#title' => t('What type of node should be created for each file?'),
          '#options' => $node_type_options,
          '#ajax' => array(
            'callback' => 'filedump_action_ajax',
            'wrapper' => 'file-field-wrapper',
            'method' => 'replace',
            'effect' => 'fade',
            'event' => 'change',
          ),
        ),
        'file_field_wrapper' => array(
          '#prefix' => '<div id="file-field-wrapper">',
          '#suffix' => '</div>'
        ),
        'continue' => array(
          '#type' => 'submit',
          '#value' => t('Continue'),
        ),
      );
    }
    
    // Step 2...Ask the user which field to attach to.
    // Triggered via ajax after a node type has been selected.
    if (!empty($form_state['values']['node_type']) && !isset($form_state['file_field'])) {
      $file_field_options = array();
      $instances = field_info_instances('node', $form_state['values']['node_type']);
      foreach ($instances as $key => $instance) {
        $info = field_info_field($key);
        if ($info['type'] == 'image' || $info['type']  == 'file') {
          $file_field_options[$key] = $instance['label'];
        }
      }
      $form['file_field_wrapper']['file_field'] = array(
        '#type' => 'select',
        '#title' => t('Which field should the files be attached to?'),
        '#options' => $file_field_options,
      );
    }
    
    // Steps 3...n: Create nodes for each file in $form_state['files']
    else if (isset($form_state['node_type']) && isset($form_state['file_field'])) {
      global $user;
      global $language;
      module_load_include('inc', 'node', 'node.pages');
      $node = new stdClass();
      $node->type = $form_state['node_type'];
      $node->language = !empty($user->language) ? $user->language : LANGUAGE_NONE;
//       $form += node_form($form, $form_state, $node);
      
      $form = array_merge_recursive($form, drupal_get_form('node_form', $node));
      
      $file = $form_state['files'][$form_state['current_file']];
      dpm($form[$form_state['file_field']][$node->language][0]['#default_value']);
      $form[$form_state['file_field']][$node->language][0]['#default_value'] = (array)$file;
//       field_attach_form('node', $node, $form, $form_state);
//       $form_state['node'] = $node;
    }
    
    return $form;
  }
  
  public function execute($form, &$form_state) {
    if (!isset($form_state['node_type']) && !isset($form_state['file_field'])) {
      $form_state['node_type'] = $form_state['values']['node_type'];
      $form_state['file_field'] = $form_state['values']['file_field'];
      $form_state['current_file'] = 0;
      $form_state['rebuild'] = TRUE;
    }
  }
}