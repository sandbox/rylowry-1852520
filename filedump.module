<?php

/**
 * Implements hook_theme().
 */
function filedump_theme() {
  return array(
    'filedump_file_upload' => array(
      'render element' => 'element',
      'template' => 'theme/filedump-file-upload',
    ),
    'filedump_file' => array(
      'render element' => 'element',
      'template' => 'theme/filedump-file'
    ),
    'filedump_file_table' => array(
      'variables' => array(
        'files' => NULL,
        'ops' => FALSE,
      ),
      'template' => 'theme/filedump-file-table',
    ),
    'filedump_file_select' => array(
      'render element' => 'element',
    ),
  );
}

function template_preprocess_filedump_file_upload(&$vars) {
  drupal_add_css(drupal_get_path('module', 'filedump') . '/css/filedump-upload.css');
  drupal_add_js(drupal_get_path('module', 'filedump') . '/js/filedump-upload.js');
}

function template_preprocess_filedump_file(&$vars) {
  drupal_add_css(drupal_get_path('module', 'filedump') . '/css/filedump-file.css');
  $element = $vars['element'];
  $file = isset($element['#file']) ? $element['#file'] : NULL;

  $vars['show_checkbox'] = isset($element['#show_checkbox']) ? $element['#show_checkbox'] : FALSE;
  $vars['show_links'] = isset($element['#show_links']) ? $element['#show_links'] : FALSE;
  $vars['file'] = $file;

  if (isset($file)) {
    if ($file->filemime == 'image/jpeg' || $file->filemime == 'image/png' || $file->filemime == 'image/gif') {
      $vars['preview'] = '<img src="' . image_style_url('thumbnail', $file->uri) . '" alt="' . $file->filename . '"/>';
    }
    else {
      // TODO: Create some generic file previews.
      $vars['preview'] = '';
    }
  }
}

/**
 * Implements hook_menu().
 */
function filedump_menu() {
  return array(
    'filedump' => array(
      'title' => t('Filedump'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('filedump_files_form'),
      'access callback' => 'user_access',
      'access arguments' => array('access filedump'),
      'type' => MENU_NORMAL_ITEM,
    ),
    'filedump/upload/ajax' => array(
      'title' => t('Filedump Upload'),
      'page callback' => 'filedump_ajax_upload',
      'access callback' => 'user_access',
      'access arguments' => array('upload filedump files'),
      'delivery callback' => 'filedump_deliver_page',
      'type' => MENU_CALLBACK,
    ),
    'admin/config/media/filedump' => array(
      'title' => t('Filedump'),
      'description' => t('Configure Filedump\'s settings, such as allowed file extensions, etc.'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('filedump_config_form'),
      'access callback' => 'filedump_access',
      'access arguments' => array('configure'),
      'file' => 'filedump.admin.inc',
      'type' => MENU_NORMAL_ITEM,
    ),
  );
}

function filedump_deliver_page($page_callback_result) {
  if (isset($page_callback_result) && is_null(drupal_get_http_header('Content-Type'))) {
  }
  drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');
  print ($page_callback_result);
}

function fildump_deliver_json($files) {
  $accepts = strtolower(str_replace(' ', '', $_SERVER['HTTP_ACCEPT']));
  if (preg_match('#^application/json.*$#', $accepts)) {
    drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
    print(json_encode($files));
  }
  else if (preg_match('#^text/html.*$#', $accepts)) {
    print('<div id="file-search-results">' . theme('filedump_file_table', array('files' => $files)) . '</div>');
  }
}

/**
 * Implements hook_permission().
 */
function filedump_permission() {
  return array(
    'configure filedump' => array(
      'title' => t('Configure Filedump'),
      'description' => t('Permission to configure Filedump.'),
    ),
    'access filedump' => array(
      'title' => t('Access Filedump'),
      'description' => t('Permission to access the Filedump page.'),
    ),
    'delete filedump files' => array(
      'title' => t('Delete Files'),
      'description' => t('Permission to delete files.'),
    ),
    'upload filedump files' => array(
      'title' => t('Upload Files'),
      'description' => t('Permission to upload files.'),
    ),
    'execute filedump actions' => array(
      'title' => t('Execute Filedump Actions'),
      'description' => t('Permission to execute actions on files.'),
    ),
  );
}

function filedump_access($op, $account = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }

  switch($op) {
    case 'upload':
      return user_access('upload filedump files', $account);
    case 'configure':
      return user_access('configure filedump', $account);
    case 'actions':
      return user_access('execute filedump actions', $account);
    case 'delete':
      return user_access('delete filedump files', $account);
    case 'browse':
      return user_access('access filedump', $account);
  }
}

function filedump_save_files() {
  global $user;
  $files = array();
  $validators = array(
    'file_validate_size' => array(variable_get('filedump_file_size')),
    'file_validate_extensions' => array(variable_get('filedump_extensions')),
  );
  foreach ($_FILES['files']['name'] as $key => $filename) {
    $file = file_save_upload($key, $validators, file_default_scheme() . '://');
    if ($file) {
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      $files[] = $file;
    }
    else {
      watchdog('filedump', 'Unable to upload file @file.', array('@file' => $filename), WATCHDOG_WARNING);
    }
  }

  return $files;
}

function filedump_ajax_upload() {
  filedump_save_files();
  $form = drupal_get_form('filedump_files_form');
  return render($form['file-list']);
}

function theme_filedump_file_select($vars) {
  $form = $vars['element'];
  $show_checkboxes = isset($form['#show_checkboxes']) && $form['#show_checkboxes'] == TRUE ? TRUE : FALSE;
  $show_links = isset($form['#show_links']) && $form['#show_links'] == TRUE ? TRUE : FALSE;

  $view_mode = isset($form['#view_mode']) ? $form['#view_mode'] : 'list';
  $output = '';
  if ($view_mode == 'list') {
    $headers = array();
    if ($show_checkboxes) {
      $headers[] = '';
    }
    $headers[] = t('File Name');
    $headers[] = t('Type');
    $headers[] = t('Date');

    $rows = array();
    foreach ($form['#files'] as $file) {
      $row = array();
      if ($show_checkboxes) {
        $row[] = theme_checkbox(array('element' => array(
            '#attributes' => array(
              'name' => 'fids[]',
              'value' => $file->fid,
            ),
          )
        ));
      }
      $row[] = l($file->filename, file_create_url($file->uri));
      $row[] = $file->filemime;

      $date = new DateTime();
      $date->setTimestamp($file->timestamp);
      $row[] = $date->format('M d, Y');
      $rows[] = $row;
    }
    $output = theme('table', array('header' => $headers, 'rows' => $rows));
  }
  else if ($view_mode == 'thumbs') {
    $slides = array();
    foreach ($form['#files'] as $file) {
      $slides[$file->fid] = array(
        '#theme' => 'filedump_file',
        '#show_links' => $show_links,
        '#show_checkbox' => $show_checkboxes,
        '#file' => $file,
      );
    }
    $output = render($slides);
  }

  return '<div class="filedump-file-list">' . $output . '</div>';
}

function filedump_filedump_file_actions() {
  return array(
    'delete' => array(
      'label' => t('Delete selected files'),
      'action' => 'DeleteFileAction',
    ),
    'create_nodes' => array(
      'label' => t('Create nodes for selected files'),
      'action' => 'CreateNodesAction',
    ),
  );
}

function filedump_begin_action($form, &$form_state) {
  // First, create and store an action in form_state
  $actions = module_invoke_all('filedump_file_actions');
  $action_name = $form_state['values']['action'];
  $action = new $actions[$action_name]['action'];
  $form_state['action'] = $action;

  // Next, store the list of files to operate on
  $files = array();
  foreach ($form_state['values']['fids'] as $fid) {
    $files[] = file_load($fid);
  }
  $form_state['files'] = $files;
  $form_state['rebuild'] = TRUE;
}

function filedump_action_ajax($form, &$form_state) {
  $action = $form_state['action'];
  return $action->ajax($form, $form_state);
}

function filedump_execute_action($form, &$form_state) {
  $action = $form_state['action'];
  $action->execute($form, $form_state);
}

function filedump_file_list() {
  $fids = db_query('select fid from {file_managed} order by timestamp desc')->fetchCol(0);
  $files = array();
  $file_options = array();
  foreach ($fids as $fid) {
    $files[] = file_load($fid);
    $file_options[$fid] = $fid;
  }

  $actions = module_invoke_all('filedump_file_actions');
  $action_options = array('' => t('Choose a file action'));
  foreach ($actions as $key => $action) {
    //TODO: Add permissions check here.
    $action_options[$key] = $action['label'];
  }

  $form['file-list'] = array(
    '#prefix' => '<div id="filedump-file-list">',
    '#suffix' => '</div>',
  );
  if (count($files) > 0) {
    $form['file-list']['file_actions'] = array(
      '#access' => filedump_access('actions'),
      'action' => array(
        '#type' => 'select',
        '#title' => t('File Actions'),
        '#title_display' => 'invisible',
        '#options' => $action_options,
      ),
      'execute' => array(
        '#type' => 'submit',
        '#value' => t('Execute'),
        '#name' => 'op',
        '#submit' => array('filedump_begin_action'),
      ),
      'file_view' => array(
        '#prefix' => '<div id="file-view">',
        '#suffix' => '</div>',
        'checkall' => array(
          '#type' => 'button',
          '#name' => 'checkall',
          '#button_type' => 'checkall',
        ),
        'slides' => array(
          '#type' => 'button',
          '#name' => 'slides',
          '#button_type' => 'slides',
        ),
        'list' => array(
          '#type' => 'button',
          '#name' => 'list',
          '#button_type' => 'list',
        ),
      ),
    );
    $form['file-list']['fids'] = array(
      '#type' => 'select',
      '#theme' => 'filedump_file_select',
      '#show_checkboxes' => filedump_access('actions'),
      '#show_links' => TRUE,
      '#view_mode' => 'thumbs',
      '#files' => $files,
      '#multiple' => TRUE,
      '#options' => $file_options,
    );
  }
  else {
    $form['file-list']['#markup'] = '<div>' . t("There aren't any files here yet.") . '</div>';
  }
  return $form;
}

function filedump_files_form($form, $form_state) {
  if ($form_state['rebuild'] === FALSE) {
    $form = array(
      '#attached' => array(
        'css' => array(drupal_get_path('module', 'filedump') . '/css/filedump-files-form.css'),
      ),
      'upload_wrapper' => array(
        '#attached' => array(
          'css' => array(
            drupal_get_path('module', 'filedump') . '/css/filedump-upload.css',
            drupal_get_path('module', 'filedump') . '/css/filedump-file.css'
          ),
          'js' => array(drupal_get_path('module', 'filedump') . '/js/filedump-upload.js'),
        ),
        '#type' => 'fieldset',
        '#title' => t('Upload Files'),
        '#access' => filedump_access('upload'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        'instructions' => array(
          '#markup' => '<div id="filedump-uploadlist"><div class="description">Drop some files onto the page to begin.</div></div>',
        ),
        'upload' => array(
          '#type' => 'submit',
          '#value' => t('Upload'),
          '#name' => 'op',
          '#submit' => array('filedump_begin_action'),
        ),
        'progress' => array(
          '#markup' => '<div id="upload-progress-wrapper"><div id="upload-progress"></div></div>'
        ),
      ),
    );
    $form += filedump_file_list();
    return $form;
  }
  else {
    $action = $form_state['action'];
    $form = $action->getForm($form, $form_state);
    $form['#submit'] = array('filedump_execute_action');
    return $form;
  }
}
