(function($) {

  Drupal.behaviors.filedump = {
    attach: function(context) {
      $('.form-checkall').click(function(e){
        e.preventDefault();
        var checkboxes = $('#filedump-file-list input:checkbox');
        checkboxes.attr('checked', !checkboxes.attr('checked'));
      });
    }
  };

  function createFilePreview(file) {
    //TODO: This needs to be smarter for non-image file uploads.
    var wrapper = $('<div class="file-wrapper"></div>');
    var file_name = $('<div class="file-name"></div>');
    var file_preview = $('<div class="file-preview"></div>');
    var file_image = $('<img/>');
    file_image.hide();

    wrapper.append(file_name);
    wrapper.append(file_preview);
    file_preview.append(file_image);

    file_name.html(file.name);

    var reader = new FileReader();
    reader.onload = function(e) {
      file_image.bind('load', function(e) {
        var width = e.target.width;
        var height = e.target.height;
        var scale = 1;

        if (width > 100 || height > 100) {
          scale = 100 / (width > height ? width : height);
        }
        file_image.attr('width', width * scale);
        file_image.attr('height', height * scale);
        file_image.show();
      });
      file_image.attr('src', e.target.result);

    }
    reader.readAsDataURL(file);
    return wrapper;
  };


  $.filedump = {
    files: new Array(),

    dragover: function(e){
      if (e.preventDefault) e.preventDefault();
      e.originalEvent.dataTransfer.dropEffect = 'move';
      return false;
    },

    drop: function(e) {
      if (e.stopPropagation) e.stopPropagation();

      var droparea = $(this);
      var files = e.originalEvent.dataTransfer.files;
      var uploadlist = $('#filedump-uploadlist');

      // If the file upload area is collapsed, expand it.
      var upload_wrapper = $('#edit-upload-wrapper');
      if (upload_wrapper.is('.collapsed')) {
        Drupal.toggleFieldset(upload_wrapper);
      }

      if (files.length > 0) {
        uploadlist.find('div.description').remove();
      }
      for (var i = 0, f; f = files[i]; i++) {
        $.filedump.files.push(f);
        uploadlist.append(createFilePreview(f));
      }
      return false;
    },

    upload: function(e) {
      e.preventDefault();
      var uploadform = $('#filedump-files-form');
      var fd = new FormData();

      // Show the progress bar and adjust its width.
      var upload_button = $('#edit-upload');
      var progress_wrapper = $('#upload-progress-wrapper');
      var progress = $('#upload-progress');
      progress_wrapper.css('display', 'inline-block');
      progress_wrapper.width(progress_wrapper.parent().width() - upload_button.outerWidth() - 19);


      for (var i = 0, f; f = $.filedump.files[i]; i++) {
        fd.append('files[]', f);
      }
      var xhr = new XMLHttpRequest();

      xhr.upload.addEventListener("progress", function(e) {
        if (e.lengthComputable) {
          $('#upload-progress').css('width', (e.loaded / e.total * 100) + '%');
        }
      }, false);

      xhr.addEventListener("load", $.filedump.load, false);
      //xhr.addEventListener("error", uploadFailed, false);
      //xhr.addEventListener("abort", uploadCanceled, false);

      xhr.open("POST", uploadform.attr('action') + '/upload/ajax');
      xhr.send(fd);
    },

    load: function(e) {
      var uploadlist = $('#filedump-uploadlist');
      uploadlist.children().remove();
      uploadlist.append('<div class="description">Drop some files onto the page to begin.</div>');
      console.log(e.target.responseText);
      $.filedump.files = new Array();
      $('#filedump-file-list').replaceWith(e.target.responseText);
      $('#upload-progress-wrapper').hide();
      Drupal.attachBehaviors($('#filedump-file-list'));
//      $('#edit-upload-wrapper').after(e.target.responseText);
    }
  };

  $(function(){
    $('body').bind('dragover', $.filedump.dragover)
    $('body').bind('drop', $.filedump.drop);
    $('#edit-upload').bind('click', $.filedump.upload);
  });

})(jQuery);
