<?php

/**
 * @file Administrative forms.
 */

function filedump_config_form() {
  $form = array(
    'filedump_extensions' => array(
      '#type' => 'textfield',
      '#title' => t('Allowed file extensions'),
      '#default_value' => variable_get('filedump_extensions'),
      '#description' => t('Separate extensions with a space or comma and do not include the leading dot.'),
      '#required' => TRUE,
    ),
    'filedump_file_size' => array(
      '#type' => 'textfield',
      '#title' => t('Allowed maximum file size'),
      '#default_value' => variable_get('filedump_file_size'),
      '#description' => t('The maximum allowed file size in bytes.'),
      '#required' => TRUE,
    ),
  );
  return system_settings_form($form);
}
